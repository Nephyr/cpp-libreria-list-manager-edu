//----------------------------------------------------------------------------->
//     Produttore: Vanzo Luca Samuele
//     Copyright 2008, Istituto Lagrange
//     Info: Libreria C, DEV - CPP
//----------------------------------------------------------------------------->

#include <stdio.h>
#include <stdlib.h>
//----------------------------------------------------------------------------->
#define STRUCT_POINTER test
typedef struct STRUCT_POINTER{
        int val;
}STRUCT_POINTER;
#include "stdlist.h"
//----------------------------------------------------------------------------->
int main(int argc, char *argv[])
{
    STDLIST *p;
    int i=0;
    //---->
    p = initList();
    p->node->val = 1;
    //---->
    p = appendChild();
    p->node->val = 3;
    //---->
    p = appendChild();
    p->node->val = 2;
    //---->
    p = removeChild();
    //---->
    p = appendChild();
    p->node->val = 5;
    //---->
    p = appendChild();
    p->node->val = 4;
    //---->        
    p = getFirst();
    //---->
    printf("\n  PRIMO = %p,\t ULTIMO = %p,\t Tot. Nodi = %d\n", getFirst(), getLast(), listLength());
    //---->
    do
    {
         printf("\n-------------------------------------------------------------------------------\n");
         printf("\n  %d) ADDRESS = %p,\t BACK = %p,\t NEXT = %p\n", getIndex(), getRunAddress(), getBackAddress(), getNextAddress());
         printf("\n  Val Nodo %d:\t%d\n  Is First:\t%s\n  Is Last:\t%s\n", getIndex(), p->node->val, (p->ISFIRST == 1) ? "Si" : "No", (p->ISLAST == 1) ? "Si" : "No");
         p = nextNode();
    }while(getRunAddress()!= NULL && isLast(getBackAddress()) != 1);
    printf("\n-------------------------------------------------------------------------------\n\n  ");
    //---->
    system("PAUSE");
    system(CLRSCR);
    //---->   
    p = getFirst();
    //---->   
    p = nextNode();
    //---->   
    p = moveLeft(FALSE);
    p = appendAfter();
    p->node->val = 20;    
    p = getFirst();
    //---->
    printf("\n  PRIMO = %p,\t ULTIMO = %p,\t Tot. Nodi = %d\n", getFirst(), getLast(), listLength());
    //---->
    do
    {
         printf("\n-------------------------------------------------------------------------------\n");
         printf("\n  %d) ADDRESS = %p,\t BACK = %p,\t NEXT = %p\n", getIndex(), getRunAddress(), getBackAddress(), getNextAddress());
         printf("\n  Val Nodo %d:\t%d\n  Is First:\t%s\n  Is Last:\t%s\n", getIndex(), p->node->val, (p->ISFIRST == 1) ? "Si" : "No", (p->ISLAST == 1) ? "Si" : "No");
         p = nextNode();
    }while(getRunAddress()!= NULL && isLast(getBackAddress()) != 1);
    printf("\n-------------------------------------------------------------------------------\n\n  ");
    //---->
    system("PAUSE");
    return 0;
}
