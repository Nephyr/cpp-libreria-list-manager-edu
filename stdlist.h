//----------------------------------------------------------------------------->
//--------------------------------| STDLIST.H |-------------------------------->
//----------------------------------------------------------------------------->
//
//     Produttore: Vanzo Luca Samuele
//     Copyright 2008, Istituto Lagrange
//     Info: Libreria C, DEV - CPP
//
//----------------------------------------------------------------------------->
#ifndef SISTEMA
#define SISTEMA 0
#endif
//----------------------------------------------------------------------------->
#ifndef TRUE
#define TRUE 1
#endif
#ifndef FALSE
#define FALSE 0
#endif
//----------------------------------------------------------------------------->
#if SISTEMA==0
#define CLRSCR "cls"
#elif SISTEMA==1
#define CLRSCR "clear"
#endif
//----------------------------------------------------------------------------->
//--------------------------------| STRUCT NODE |------------------------------>
//----------------------------------------------------------------------------->
typedef struct DBTH_LIST{
    int ISFIRST;
    struct DBTH_LIST *BACK;
    struct DBTH_LIST *NEXT;
    int ISLAST;
    int INDEX;
    struct STRUCT_POINTER *node;
}STDLIST;
typedef struct DBTH_LIST_FL{
    struct DBTH_LIST *DBL_pID_FIRST;
    struct DBTH_LIST *DBL_pID_RUNTIME;
    struct DBTH_LIST *DBL_pID_LAST;
    int pID_Counter;
}DBTH_LISTS_FL;
//----------------------------------------------------------------------------->
DBTH_LISTS_FL DBTH_LIST_FL;
//----------------------------------------------------------------------------->
//--------------------------------| PROTOTIPI |-------------------------------->
//----------------------------------------------------------------------------->
int listLength(void);
//----------------------------------------------------------------------------->
int getIndex(void);
void setIndex(int);
//----------------------------------------------------------------------------->
void nodesConcat(void);
//----------------------------------------------------------------------------->
int isFirst(struct DBTH_LIST *);
int isLast(struct DBTH_LIST *);
//----------------------------------------------------------------------------->
struct DBTH_LIST *getFirst(void);
struct DBTH_LIST *getLast(void);
//----------------------------------------------------------------------------->
struct DBTH_LIST *moveLeft(int);
struct DBTH_LIST *moveRight(int);
//----------------------------------------------------------------------------->
struct DBTH_LIST *nextNode(void);
struct DBTH_LIST *previousNode(void);
//----------------------------------------------------------------------------->
struct DBTH_LIST *removeChild(void);
struct DBTH_LIST *appendChild(void);
struct DBTH_LIST *appendBefore(void);
struct DBTH_LIST *appendAfter(void);
//----------------------------------------------------------------------------->
struct DBTH_LIST *initList(void);
//----------------------------------------------------------------------------->
struct DBTH_LIST *getBackAddress(void);
struct DBTH_LIST *getRunAddress(void);
struct DBTH_LIST *getNextAddress(void);
//----------------------------------------------------------------------------->
//-----------------------------| CORPO  FUNZIONI |----------------------------->
//----------------------------------------------------------------------------->
int listLength()
{
    return DBTH_LIST_FL.pID_Counter;
}
//----------------------------------------------------------------------------->
int getIndex()
{
    return DBTH_LIST_FL.DBL_pID_RUNTIME->INDEX;
}
//----------------------------------------------------------------------------->
void setIndex(int nID)
{
    DBTH_LIST_FL.DBL_pID_RUNTIME->INDEX = nID;
}
//----------------------------------------------------------------------------->
void nodesConcat()
{
    STDLIST *pID = DBTH_LIST_FL.DBL_pID_FIRST, *tmp = DBTH_LIST_FL.DBL_pID_LAST;
    if(pID->BACK == NULL)
    {
         pID->BACK = tmp;
         tmp->NEXT = pID;
    }
    else
    {
         pID->BACK = NULL;
         tmp->NEXT = NULL;
    }
}
//----------------------------------------------------------------------------->
struct DBTH_LIST *getBackAddress()
{
    return (DBTH_LIST_FL.DBL_pID_RUNTIME->BACK);
}
//----------------------------------------------------------------------------->
struct DBTH_LIST *getRunAddress()
{
    return (DBTH_LIST_FL.DBL_pID_RUNTIME);
}
//----------------------------------------------------------------------------->
struct DBTH_LIST *getNextAddress()
{
    return (DBTH_LIST_FL.DBL_pID_RUNTIME->NEXT);
}
//----------------------------------------------------------------------------->
int isFirst(struct DBTH_LIST *pID)
{
    return pID->ISFIRST;
}
//----------------------------------------------------------------------------->
int isLast(struct DBTH_LIST *pID)
{
    return pID->ISLAST;
}
//----------------------------------------------------------------------------->
struct DBTH_LIST *getFirst()
{
    DBTH_LIST_FL.DBL_pID_RUNTIME = DBTH_LIST_FL.DBL_pID_FIRST;
    return (DBTH_LIST_FL.DBL_pID_FIRST);
}
//----------------------------------------------------------------------------->
struct DBTH_LIST *getLast()
{
    DBTH_LIST_FL.DBL_pID_RUNTIME = DBTH_LIST_FL.DBL_pID_LAST;
    return (DBTH_LIST_FL.DBL_pID_LAST);
}
//----------------------------------------------------------------------------->
struct DBTH_LIST *moveRight(int BOOL_T)
{
    STDLIST *pID = DBTH_LIST_FL.DBL_pID_RUNTIME;
    STRUCT_POINTER *tmp;
    int vID;
    //---->
    tmp = pID->node;
    if(BOOL_T) vID = pID->INDEX;
    pID->node = pID->NEXT->node;
    if(BOOL_T) pID->INDEX = pID->NEXT->INDEX;
    pID->NEXT->node = tmp;
    if(BOOL_T) pID->NEXT->INDEX = vID;
    DBTH_LIST_FL.DBL_pID_RUNTIME = pID->NEXT;
    return (DBTH_LIST_FL.DBL_pID_RUNTIME);
}
//----------------------------------------------------------------------------->
struct DBTH_LIST *moveLeft(int BOOL_T)
{
    STDLIST *pID = DBTH_LIST_FL.DBL_pID_RUNTIME;
    STRUCT_POINTER *tmp;
    int vID;
    //---->
    tmp = pID->node;
    if(BOOL_T) vID = pID->INDEX;
    pID->node = pID->BACK->node;
    if(BOOL_T) pID->INDEX = pID->BACK->INDEX;
    pID->BACK->node = tmp;
    if(BOOL_T) pID->BACK->INDEX = vID;
    DBTH_LIST_FL.DBL_pID_RUNTIME = pID->BACK;
    return (DBTH_LIST_FL.DBL_pID_RUNTIME);
}
//----------------------------------------------------------------------------->
struct DBTH_LIST *nextNode()
{
    DBTH_LIST_FL.DBL_pID_RUNTIME = DBTH_LIST_FL.DBL_pID_RUNTIME->NEXT;
    return (DBTH_LIST_FL.DBL_pID_RUNTIME);
}
//----------------------------------------------------------------------------->
struct DBTH_LIST *previousNode()
{
    DBTH_LIST_FL.DBL_pID_RUNTIME = DBTH_LIST_FL.DBL_pID_RUNTIME->BACK;
    return (DBTH_LIST_FL.DBL_pID_RUNTIME);
}
//----------------------------------------------------------------------------->
struct DBTH_LIST *removeChild()
{
    STDLIST *pID = DBTH_LIST_FL.DBL_pID_RUNTIME, *tmpb;
    tmpb = (pID->NEXT == NULL) ? pID->BACK : pID->NEXT;
    free(pID->node);
    if(pID->NEXT != NULL && pID->BACK == NULL)
    {
         pID->NEXT->BACK = NULL;
         pID->NEXT->ISFIRST = 1;
         DBTH_LIST_FL.DBL_pID_RUNTIME = tmpb;
         free(pID);
         DBTH_LIST_FL.DBL_pID_FIRST = tmpb;
         DBTH_LIST_FL.pID_Counter--;
    }
    else if(pID->NEXT == NULL && pID->BACK != NULL)
    {
         pID->BACK->NEXT = NULL;
         pID->BACK->ISLAST = 1;
         DBTH_LIST_FL.DBL_pID_RUNTIME = tmpb;
         free(pID);
         DBTH_LIST_FL.DBL_pID_LAST = tmpb;
         DBTH_LIST_FL.pID_Counter--;
    }
    else if(pID->NEXT != NULL && pID->BACK != NULL)
    {
         pID->BACK->NEXT = pID->NEXT;
         pID->NEXT->BACK = pID->BACK;
         DBTH_LIST_FL.DBL_pID_RUNTIME = tmpb;
         free(pID);
         DBTH_LIST_FL.pID_Counter--;
    }
    return tmpb;
}
//----------------------------------------------------------------------------->
struct DBTH_LIST *initList()
{
    STDLIST *pID;
    //---->
    if((pID = (STDLIST *)calloc(1, sizeof(STDLIST))) == NULL)
    {
         system(CLRSCR);
         printf("\n\n\n\n\t Memory Error!!\n\t ");
         system("pause");
         exit(1);
    }
    if((pID->node = (STRUCT_POINTER *)calloc(1, sizeof(STRUCT_POINTER))) == NULL)
    {
         system(CLRSCR);
         printf("\n\n\n\n\t Memory Error!!\n\t ");
         system("pause");
         exit(1);
    }
    //---->
    pID->ISFIRST = 1;
    pID->BACK = NULL;
    pID->NEXT = NULL;
    pID->ISLAST = 1;
    pID->INDEX = 0;
    //---->
    DBTH_LIST_FL.DBL_pID_FIRST = pID;
    DBTH_LIST_FL.DBL_pID_RUNTIME = pID;
    DBTH_LIST_FL.DBL_pID_LAST = pID;
    DBTH_LIST_FL.pID_Counter = 1;
    //---->
    return (pID);
}
//----------------------------------------------------------------------------->
struct DBTH_LIST *appendChild(void)
{
    STDLIST *pID = DBTH_LIST_FL.DBL_pID_LAST;
    if((pID->NEXT = (STDLIST *)calloc(1, sizeof(STDLIST))) == NULL)
    {
         system(CLRSCR);
         printf("\n\n\n\n\t Memory Error!!\n\t ");
         system("pause");
         exit(1);
    }
    if((pID->NEXT->node = (STRUCT_POINTER *)calloc(1, sizeof(STRUCT_POINTER))) == NULL)
    {
         system(CLRSCR);
         printf("\n\n\n\n\t Memory Error!!\n\t ");
         system("pause");
         exit(1);
    }
    //---->
    pID->NEXT->INDEX = listLength();
    pID->NEXT->ISFIRST = 0;
    pID->NEXT->BACK = pID;
    pID->NEXT->NEXT = NULL;
    pID->ISLAST = 0;
    pID->NEXT->ISLAST = 1;
    //---->
    DBTH_LIST_FL.DBL_pID_LAST = pID->NEXT;
    DBTH_LIST_FL.DBL_pID_RUNTIME = pID->NEXT;
    DBTH_LIST_FL.pID_Counter++;
    //---->
    return (pID->NEXT);
}
//----------------------------------------------------------------------------->
struct DBTH_LIST *appendBefore(void)
{
    STDLIST *pID = DBTH_LIST_FL.DBL_pID_RUNTIME, *tmpb = pID->BACK;
    if((pID->BACK = (STDLIST *)calloc(1, sizeof(STDLIST))) == NULL)
    {
         system(CLRSCR);
         printf("\n\n\n\n\t Memory Error!!\n\t ");
         system("pause");
         exit(1);
    }
    if((pID->BACK->node = (STRUCT_POINTER *)calloc(1, sizeof(STRUCT_POINTER))) == NULL)
    {
         system(CLRSCR);
         printf("\n\n\n\n\t Memory Error!!\n\t ");
         system("pause");
         exit(1);
    }
    //---->
    pID->BACK->INDEX = listLength();
    pID->BACK->ISFIRST = (pID->ISFIRST == 1) ? 1 : 0;
    pID->ISFIRST = 0;
    if(tmpb != NULL) tmpb->NEXT = pID->BACK;
    pID->BACK->BACK = (tmpb != NULL) ? tmpb : NULL;
    pID->BACK->NEXT = pID;
    pID->BACK->ISLAST = 0;
    //---->
    DBTH_LIST_FL.DBL_pID_RUNTIME = pID->BACK;
    DBTH_LIST_FL.DBL_pID_FIRST = (pID->BACK->BACK == NULL) ? pID->BACK : DBTH_LIST_FL.DBL_pID_FIRST;
    DBTH_LIST_FL.pID_Counter++;
    //---->
    return (pID->BACK);
}
//----------------------------------------------------------------------------->
struct DBTH_LIST *appendAfter(void)
{
    STDLIST *pID = DBTH_LIST_FL.DBL_pID_RUNTIME, *tmpb = pID->NEXT;
    if((pID->NEXT = (STDLIST *)calloc(1, sizeof(STDLIST))) == NULL)
    {
         system(CLRSCR);
         printf("\n\n\n\n\t Memory Error!!\n\t ");
         system("pause");
         exit(1);
    }
    if((pID->NEXT->node = (STRUCT_POINTER *)calloc(1, sizeof(STRUCT_POINTER))) == NULL)
    {
         system(CLRSCR);
         printf("\n\n\n\n\t Memory Error!!\n\t ");
         system("pause");
         exit(1);
    }
    //---->
    pID->NEXT->INDEX = listLength();
    pID->NEXT->ISLAST = (pID->ISLAST == 1) ? 1 : 0;
    pID->ISLAST = 0;
    if(tmpb != NULL) tmpb->BACK = pID->NEXT;
    pID->NEXT->NEXT = (tmpb != NULL) ? tmpb : NULL;
    pID->NEXT->BACK = pID;
    pID->NEXT->ISFIRST = 0;
    //---->
    DBTH_LIST_FL.DBL_pID_RUNTIME = pID->NEXT;
    DBTH_LIST_FL.DBL_pID_LAST = (pID->NEXT->NEXT == NULL) ? pID->NEXT : DBTH_LIST_FL.DBL_pID_LAST;
    DBTH_LIST_FL.pID_Counter++;
    //---->
    return (pID->NEXT);
}
//----------------------------------------------------------------------------->
